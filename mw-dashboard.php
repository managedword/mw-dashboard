<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/managedword/mw-dashboard
 * @since             1.0.0
 * @package           MW_Dashboard
 *
 * @wordpress-plugin
 * Plugin Name:       Managed Word Dashboard
 * Plugin URI:        https://bitbucket.org/managedword/mw-dashboard
 * Description:       End user and system management dashboard for Managed Word hosting.
 * Version:           1.0.0
 * Author:            Managed Word, Christopher Frazier <chris@fraziermedia.com>
 * Author URI:        https://www.managedword.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mw-dashboard
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-mw-dashboard-activator.php
 */
function activate_mw_dashboard() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mw-dashboard-activator.php';
	MW_Dashboard_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-mw-dashboard-deactivator.php
 */
function deactivate_mw_dashboard() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mw-dashboard-deactivator.php';
	MW_Dashboard_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_mw_dashboard' );
register_deactivation_hook( __FILE__, 'deactivate_mw_dashboard' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-mw-dashboard.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_mw_dashboard() {

	$plugin = new MW_Dashboard();
	$plugin->run();

}
run_mw_dashboard();
