<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/managedword/mw-dashboard
 * @since      1.0.0
 *
 * @package    MW_Dashboard
 * @subpackage MW_Dashboard/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    MW_Dashboard
 * @subpackage MW_Dashboard/includes
 * @author     Christopher Frazier <chris@fraziermedia.com>
 */
class MW_Dashboard_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
