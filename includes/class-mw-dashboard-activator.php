<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bitbucket.org/managedword/mw-dashboard
 * @since      1.0.0
 *
 * @package    MW_Dashboard
 * @subpackage MW_Dashboard/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    MW_Dashboard
 * @subpackage MW_Dashboard/includes
 * @author     Christopher Frazier <chris@fraziermedia.com>
 */
class MW_Dashboard_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
